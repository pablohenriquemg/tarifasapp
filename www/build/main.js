webpackJsonp([0],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EletronicUnitPostPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eletronic_units_eletronic_units__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__next_next__ = __webpack_require__(339);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EletronicUnitPostPage = /** @class */ (function () {
    function EletronicUnitPostPage(navCtrl, plt, dialogs, globals) {
        this.navCtrl = navCtrl;
        this.plt = plt;
        this.dialogs = dialogs;
        this.globals = globals;
        this.institutionName = '';
        this.address = '';
        this.city = '';
        this.isNullOrEmpty = function (value) {
            return (!value || value == undefined || value == "" || value.length == 0);
        };
    }
    EletronicUnitPostPage.prototype.startSearch = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            if (!this.isNullOrEmpty(this.institutionName) || !this.isNullOrEmpty(this.address) || !this.isNullOrEmpty(this.city)) {
                this.globals.ELETRONIC_UNIT_SEARCH = { 'institution': this.institutionName, 'address': this.address, 'city': this.city };
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__eletronic_units_eletronic_units__["a" /* EletronicUnitsPage */], {});
            }
            else {
                this.dialogs.alert(this.globals.FILL_ONLY_ONE_FIELD, this.globals.OPS);
            }
        }
    };
    EletronicUnitPostPage.prototype.searchNear = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__next_next__["a" /* NextPage */], {});
        }
    };
    EletronicUnitPostPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\eletronic-unit-post\eletronic-unit-post.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                    <h4 class="content-title">POSTOS DE ATENDIMENTO ELETRÔNICO</h4>\n\n                    <p class="texto-obs">\n\n                        Você pode preencher um ou mais campo(s) abaixo. Tentaremos localizar os postos de atendimento eletrônico(s) nos critérios da pesquisa.\n\n                    </p>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-item>\n\n                    <ion-label>Instituição</ion-label>\n\n                    <ion-input [(ngModel)]="institutionName" placeholder="Ex.: Banco do Brasil"></ion-input>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Logradouro</ion-label>\n\n                    <ion-input [(ngModel)]="address" placeholder="Ex.: Rua Santa Catarina"></ion-input>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Município</ion-label>\n\n                    <ion-input [(ngModel)]="city" placeholder="Ex.: Belo Horizonte"></ion-input>\n\n                </ion-item>\n\n                <br/>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <button ion-button full (click)=\'startSearch();\'>Localizar</button>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <!--<ion-row>\n\n            <ion-col col-12>\n\n                    <h4>OU LOCALIZAR TODOS OS MAIS PRÓXIMOS</h4>\n\n                    <p class="texto-obs">\n\n                        Toque no botão abaixo para tentar localizar TODOS os postos de atendimento eletrônico mais próximos de você.\n\n                    </p>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <button ion-button full (click)=\'searchNear();\'>Localizar todos</button>\n\n            </ion-col>\n\n        </ion-row> -->\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\eletronic-unit-post\eletronic-unit-post.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_4__shared_globals__["a" /* Globals */]])
    ], EletronicUnitPostPage);
    return EletronicUnitPostPage;
}());

//# sourceMappingURL=eletronic-unit-post.js.map

/***/ }),

/***/ 117:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 117;

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Globals; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Globals = /** @class */ (function () {
    function Globals() {
        this.CONSOLIDATED_GROUP_SELECTED = {};
        this.PERSON_TYPE_SELECTED = { id: 1, item: 'Pessoa Física', desc: 'F' };
        this.INSTITUTION_SELECTED = {};
        this.ELETRONIC_UNIT_SEARCH = {};
        this.URL_API_BCB_URL = 'http://api.bcb.gov.br/';
        this.BASE_URL_END_POINT = 'https://olinda.bcb.gov.br/olinda/servico/';
        this.URL_SELIC_DAY_RATE = 'dados/serie/bcdata.sgs.11/dados/ultimos/30';
        this.URL_SELIC_MONTH_RATE = 'dados/serie/bcdata.sgs.4390/dados/ultimos/12';
        this.URL_SELIC_YEAR_RATE = 'dados/serie/bcdata.sgs.1178/dados/ultimos/10';
        this.URL_LAST_SELIC_RATE = 'dados/serie/bcdata.sgs.1178/dados/ultimos/365';
        this.URL_LAST_IPCA_RATE = 'dados/serie/bcdata.sgs.4447/dados/ultimos/20';
        this.URL_OMBUDSMAN_CONTACT = 'Informes_Ouvidorias/versao/v1/odata/Ouvidorias';
        this.URL_ELETRONIC_UNITS = 'Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico';
        this.URL_CONSOLIDATED_INSTITUTIONS = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/ListaInstituicoesDeGrupoConsolidado(CodigoGrupoConsolidado=@CodigoGrupoConsolidado)';
        this.URL_CONSOLIDATED_GROUPS = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/GruposConsolidados';
        this.URL_RATES_BY_INSTITUTION = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/ListaTarifasPorInstituicaoFinanceira(PessoaFisicaOuJuridica=@PessoaFisicaOuJuridica,CNPJ=@CNPJ)';
        this.URL_VALUES_SERVICES_BANKING = 'Informes_ListaValoresDeServicoBancario/versao/v1/odata/ListaValoresServicoBancario(PessoaFisicaOuJuridica=@PessoaFisicaOuJuridica,CodigoGrupoConsolidado=@CodigoGrupoConsolidado)';
        this.ATTENTION = 'Atenção';
        this.OPS = 'Ops...';
        this.PROGRESS_MESSAGE = 'Obtendo dados. Aguarde...';
        this.FAIL_REQUEST_MESSAGE = 'Falha ao obter dados. Por favor, tente novamente mais tarde.';
        this.FILL_ALL_FIELDS = 'Preencha todos os campos.';
        this.NO_ELETRONIC_UNIT_FIND = 'Não foi encontrado nenhum resultado com os critérios de busca.';
        this.NO_CONSOLIDATED_GROUPS = 'Não foi possível obter os grupos consolidados, por favor tente mais tarde.';
        this.FAIL_TO_GET_RATE = 'Falha para obter as taxas. Por favor, tente novamente mais tarde.';
        this.FAIL_TO_GET_INSTITUTIONS = 'Falha para obter as instituições. Por favor, tente novamente mais tarde.';
        this.FILL_ONLY_ONE_FIELD = 'Preencha pelo menos um critério de busca.';
        this.NO_RATE_INSTITUTION = 'Não foram localizadas tarifas para essa instituição.';
    }
    Globals = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], Globals);
    return Globals;
}());

//# sourceMappingURL=globals.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RequestService = /** @class */ (function () {
    function RequestService(http, loadingController, globals, dialogs) {
        this.http = http;
        this.loadingController = loadingController;
        this.globals = globals;
        this.dialogs = dialogs;
        this.isNullOrEmpty = function (value) {
            return (!value || value == undefined || value == "" || value.length == 0);
        };
    }
    RequestService.prototype.getAllConsolidatedGroups = function () {
        var _this = this;
        var completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_CONSOLIDATED_GROUPS;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, { 'format': 'JSON' }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.value.length < 1) {
                    _this.dialogs.alert(_this.globals.NO_CONSOLIDATED_GROUPS, _this.globals.OPS);
                }
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getLastSelicRate = function () {
        var _this = this;
        var completeUrl = this.globals.URL_API_BCB_URL + this.globals.URL_LAST_SELIC_RATE;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, { 'format': 'JSON' }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.length < 1) {
                    _this.dialogs.alert(_this.globals.FAIL_TO_GET_RATE, _this.globals.OPS);
                }
                resolve(jsondata);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getLastIpcaRate = function () {
        var _this = this;
        var completeUrl = this.globals.URL_API_BCB_URL + this.globals.URL_LAST_IPCA_RATE;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, { 'format': 'JSON' }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.length < 1) {
                    _this.dialogs.alert(_this.globals.FAIL_TO_GET_RATE, _this.globals.OPS);
                }
                resolve(jsondata);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getOmbudsmanContact = function (ombudsmanName) {
        var _this = this;
        var filterParam = "contains(Nome," + "'" + ombudsmanName + "'" + ")";
        var completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_OMBUDSMAN_CONTACT;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, {
                '$filter': filterParam,
                'top': '10', 'format': 'JSON',
                '$select': 'CNPJ,Nome,Ouvidor,WebSite,Telefone'
            }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.value.length < 1) {
                    _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS);
                }
                jsondata.value.forEach(function (item) {
                    var cnpj = item.CNPJ.substring(0, 2).concat(".");
                    cnpj += item.CNPJ.substring(2, 5).concat(".");
                    cnpj += item.CNPJ.substring(5, 8).concat("/****-**");
                    item.CNPJ = cnpj;
                });
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getInstitutionsByConsolidatedGroup = function (groupCode) {
        var _this = this;
        var paramGroupCode = "'".concat(groupCode).concat("'");
        var completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_CONSOLIDATED_INSTITUTIONS;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, {
                'top': '100', 'format': 'JSON',
                '$select': 'Cnpj,Nome',
                '@CodigoGrupoConsolidado': paramGroupCode
            }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.length < 1) {
                    _this.dialogs.alert(_this.globals.FAIL_TO_GET_INSTITUTIONS, _this.globals.OPS);
                }
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getRateByInstitutions = function (personType, cnpj) {
        var _this = this;
        var personSelected = "'".concat(personType).concat("'");
        var cnpjSelected = "'".concat(cnpj).concat("'");
        var completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_RATES_BY_INSTITUTION;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, {
                'format': 'JSON',
                '@PessoaFisicaOuJuridica': personSelected,
                '@CNPJ': cnpjSelected
            }, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.length < 1) {
                    _this.dialogs.alert(_this.globals.FAIL_TO_GET_RATE, _this.globals.OPS);
                }
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.getEletronicUnits = function (institution, address, city) {
        var _this = this;
        var filter = this.makeFilter(encodeURI(institution), encodeURI(address), encodeURI(city));
        var completeUrl = "https://olinda.bcb.gov.br/olinda/servico/Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico?$top=30&$format=JSON&$select=Cnpj,NomeIf,Segmento,NomePosto,TipoPosto,Endereco,Numero,Complemento,Bairro,Cep,MunicipioIbge,Municipio,UF,CnpjAssist,NomeAssist&$filter=";
        completeUrl = completeUrl.concat(filter);
        //let filter = this.makeFilter(encodeURI(institution), encodeURI(address), encodeURI(city));
        //let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_ELETRONIC_UNITS;
        var loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, {}, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.value.length == 0) {
                    _this.dialogs.alert(_this.globals.NO_ELETRONIC_UNIT_FIND, _this.globals.OPS);
                }
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.makeFilter = function (institution, address, city) {
        var filterInstitution = "";
        var filterAddress = "";
        var filterCity = "";
        if (!this.isNullOrEmpty(institution)) {
            filterInstitution = "contains(NomeIf,".concat("'").concat(institution.toUpperCase()).concat("')");
        }
        if (!this.isNullOrEmpty(address)) {
            filterAddress = "contains(Endereco,".concat("'").concat(address.toUpperCase()).concat("')");
        }
        if (!this.isNullOrEmpty(city)) {
            filterCity = "contains(Municipio,".concat("'").concat(city.toUpperCase()).concat("')");
        }
        var filter = "";
        if (!this.isNullOrEmpty(filterInstitution)) {
            filter = filterInstitution;
            if (!this.isNullOrEmpty(filterAddress)) {
                filter = filter.concat(" and ").concat(filterAddress);
            }
            if (!this.isNullOrEmpty(filterCity)) {
                filter = filter.concat(" and ").concat(filterCity);
            }
            return filter;
        }
        if (!this.isNullOrEmpty(filterAddress)) {
            filter = filterAddress;
            if (!this.isNullOrEmpty(filterInstitution)) {
                filter = filter.concat(" and ").concat(filterInstitution);
            }
            if (!this.isNullOrEmpty(filterCity)) {
                filter = filter.concat(" and ").concat(filterCity);
            }
            return filter;
        }
        if (!this.isNullOrEmpty(filterCity)) {
            filter = filterCity;
            if (!this.isNullOrEmpty(filterInstitution)) {
                filter = filter.concat(" and ").concat(filterInstitution);
            }
            if (!this.isNullOrEmpty(filterAddress)) {
                filter = filter.concat(" and ").concat(filterAddress);
            }
            return filter;
        }
    };
    RequestService.prototype.getEletronicUnitsByAddressAndCity = function (address, city) {
        var _this = this;
        var filter = this.makeFilterForSimpleAddress(encodeURI(address), encodeURI(city));
        var completeUrl = "https://olinda.bcb.gov.br/olinda/servico/Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico?$top=30&$format=JSON&$select=Cnpj,NomeIf,Segmento,NomePosto,TipoPosto,Endereco,Numero,Complemento,Bairro,Cep,MunicipioIbge,Municipio,UF,CnpjAssist,NomeAssist&$filter=";
        completeUrl = completeUrl.concat(filter);
        var loading = this.loadingController.create({ content: 'Obtendo os postos eletrônicos próximos...' });
        loading.present();
        return new Promise(function (resolve, reject) {
            _this.http.get(completeUrl, {}, {})
                .then(function (data) {
                loading.dismissAll();
                var jsondata = JSON.parse(data.data);
                if (jsondata.value.length == 0) {
                    _this.dialogs.alert('Não foi encontrado nenhum posto eletrônico próximo, por favor tente outra opção de pesquisa.', 'Ops');
                }
                resolve(jsondata.value);
            })
                .catch(function (error) {
                loading.dismissAll();
                reject(error.error);
            });
        });
    };
    RequestService.prototype.makeFilterForSimpleAddress = function (address, city) {
        var filterAddress = "";
        var filterCity = "";
        if (!this.isNullOrEmpty(address)) {
            filterAddress = "contains(Endereco,".concat("'").concat(address.toUpperCase()).concat("')");
        }
        if (!this.isNullOrEmpty(city)) {
            filterCity = "contains(Municipio,".concat("'").concat(city.toUpperCase()).concat("')");
        }
        return filterAddress.concat(" and ").concat(filterCity);
    };
    RequestService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__["a" /* Dialogs */]])
    ], RequestService);
    return RequestService;
}());

//# sourceMappingURL=req-service.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__consolidated_groups_consolidated_groups__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__select_rate_select_rate__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ombudsman_contact_ombudsman_contact__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__eletronic_unit_post_eletronic_unit_post__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage() {
        // this tells the tabs component which Pages
        // should be each tab's root Page
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__ombudsman_contact_ombudsman_contact__["a" /* OmbudsmanContactPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__eletronic_unit_post_eletronic_unit_post__["a" /* EletronicUnitPostPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_2__select_rate_select_rate__["a" /* SelectRatePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\tabs\tabs.html"*/'<ion-tabs>\n\n    <ion-tab [root]="tab1Root" tabTitle="Grupos" tabIcon="list"></ion-tab>\n\n    <ion-tab [root]="tab2Root" tabTitle="Ouvidorias" tabIcon="call"></ion-tab>\n\n    <ion-tab [root]="tab3Root" tabTitle="Atendimento" tabIcon="pin"></ion-tab>\n\n    <ion-tab [root]="tab4Root" tabTitle="Taxas" tabIcon="trending-up"></ion-tab>\n\n</ion-tabs>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\tabs\tabs.html"*/ }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstitutionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__rates_institutions_rate_institution__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__consolidated_groups_consolidated_groups__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var InstitutionPage = /** @class */ (function () {
    function InstitutionPage(navCtrl, dialogs, globals, rqService) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.rqService = rqService;
    }
    InstitutionPage.prototype.ionViewDidLoad = function () {
        this.getAllItems();
    };
    InstitutionPage.prototype.getAllItems = function () {
        var _this = this;
        var groupCode = this.globals.CONSOLIDATED_GROUP_SELECTED.Codigo;
        this.rqService.getInstitutionsByConsolidatedGroup(groupCode).then(function (data) {
            _this.institutions = data;
            _this.temporary_institutions = data;
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS)
                .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */]); });
        });
    };
    InstitutionPage.prototype.itemTapped = function (institution) {
        this.globals.INSTITUTION_SELECTED = institution;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__rates_institutions_rate_institution__["a" /* RateInstitutionPage */], {});
    };
    InstitutionPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.institutions = this.temporary_institutions;
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.institutions = this.institutions.filter(function (item) {
                return (item.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    InstitutionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\institutions\institution.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n    <ion-searchbar (ionInput)="getItems($event)" placeholder="Pesquisar instituição..."></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <h4 class="content-title">SELECIONE UMA INSTITUIÇÃO</h4>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-list>\n\n                    <button text-wrap ion-item *ngFor="let inst of institutions" (click)="itemTapped(inst)">\n\n                        {{ inst.Nome }}\n\n                    </button>\n\n                </ion-list>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\institutions\institution.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */], __WEBPACK_IMPORTED_MODULE_4__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_5__shared_req_service__["a" /* RequestService */]])
    ], InstitutionPage);
    return InstitutionPage;
}());

//# sourceMappingURL=institution.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RateInstitutionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__consolidated_groups_consolidated_groups__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RateInstitutionPage = /** @class */ (function () {
    function RateInstitutionPage(navCtrl, dialogs, globals, rqService) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.rqService = rqService;
    }
    RateInstitutionPage.prototype.ionViewDidLoad = function () {
        this.selected_institution = this.globals.INSTITUTION_SELECTED.Nome;
        this.selected_person = this.globals.PERSON_TYPE_SELECTED.item;
        this.getAllItems();
    };
    RateInstitutionPage.prototype.getAllItems = function () {
        var _this = this;
        var personType = this.globals.PERSON_TYPE_SELECTED.desc;
        var cnpj = this.globals.INSTITUTION_SELECTED.Cnpj;
        this.rqService.getRateByInstitutions(personType, cnpj).then(function (data) {
            if (data.length === 0) {
                _this.dialogs.alert(_this.globals.NO_RATE_INSTITUTION, _this.globals.OPS)
                    .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */]); });
            }
            else {
                data.forEach(function (item) { item.Periodicidade = item.Periodicidade.toUpperCase(); });
                _this.institution_rates = data;
                _this.temporary_institution_rates = data;
            }
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS)
                .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */]); });
        });
    };
    RateInstitutionPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.institution_rates = this.temporary_institution_rates;
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.institution_rates = this.institution_rates.filter(function (item) {
                return (item.Servico.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    RateInstitutionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\rates-institutions\rate-institution.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n    <ion-searchbar (ionInput)="getItems($event)" placeholder="Pesquisar"></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <h5 class="page-title">{{selected_institution}}</h5>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-list>\n\n                    <ion-list-header>\n\n                        Tarifas para {{selected_person}}\n\n                    </ion-list-header>\n\n                    <ion-item text-wrap *ngFor="let inst of institution_rates">\n\n                        <h3>{{inst.Servico}}</h3>\n\n                        <p><strong>Valor máximo:</strong>&nbsp; {{inst.ValorMaximo | currency :\'BRL\'}}</p>\n\n                        <p><strong>Periodicidade:</strong>&nbsp;{{inst.Periodicidade}}</p>\n\n                        <p><strong>Data de vigência:</strong>&nbsp;{{inst.DataVigencia | amDateFormat: \'DD/MM/YYYY\'}}</p>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\rates-institutions\rate-institution.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_3__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_4__shared_req_service__["a" /* RequestService */]])
    ], RateInstitutionPage);
    return RateInstitutionPage;
}());

//# sourceMappingURL=rate-institution.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_shared_globals__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PopoverComponent = /** @class */ (function () {
    function PopoverComponent(viewCtrl, globals, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.globals = globals;
        this.toastCtrl = toastCtrl;
    }
    PopoverComponent.prototype.ionViewDidLoad = function () {
        this.items = [
            { id: 1, item: 'Pessoa Física', desc: 'F' },
            { id: 2, item: 'Pessoa Jurídica', desc: 'J' },
        ];
    };
    PopoverComponent.prototype.itemClick = function (obj) {
        this.presentToast(obj.item);
        this.globals.PERSON_TYPE_SELECTED = obj;
        this.viewCtrl.dismiss(obj);
    };
    PopoverComponent.prototype.presentToast = function (selectedPerson) {
        var toast = this.toastCtrl.create({
            message: selectedPerson.concat(' selecionada.'),
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    PopoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({ selector: 'popover',template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\components\popover\popover.html"*/'<ion-list>\n\n 	<ion-item *ngFor="let item of items" (click)="itemClick(item)">\n\n 	<ion-icon name="briefcase"></ion-icon>\n\n 		{{item.item}}\n\n	</ion-item>\n\n</ion-list>\n\n'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\components\popover\popover.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__pages_shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], PopoverComponent);
    return PopoverComponent;
}());

//# sourceMappingURL=popover.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelicPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__select_rate_select_rate__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__selic_selic_modal__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SelicPage = /** @class */ (function () {
    function SelicPage(navCtrl, dialogs, globals, plt, rqService, modalCtrl) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.plt = plt;
        this.rqService = rqService;
        this.modalCtrl = modalCtrl;
    }
    SelicPage.prototype.ionViewDidLoad = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getSelicRateList();
        }
    };
    SelicPage.prototype.getSelicRateList = function () {
        var _this = this;
        this.rqService.getLastSelicRate().then(function (data) {
            _this.selicRateList = data;
            _this.buildChart(_this.getSampleResult(_this.selicRateList));
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS)
                .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__select_rate_select_rate__["a" /* SelectRatePage */]); });
        });
    };
    SelicPage.prototype.getSampleResult = function (arr) {
        var arrFiltered = Array();
        arr = arr.reverse();
        for (var i = 0; i < arr.length; i += 30) {
            arrFiltered.push(arr[i]);
        }
        return arrFiltered.reverse();
    };
    SelicPage.prototype.buildChart = function (listRate) {
        var listData = listRate.map(function (a) { return parseFloat(a.valor); });
        var listCategories = listRate.map(function (a) { return a.data; });
        var actual = 'Taxa atual de '.concat(listData[listData.length - 1]).concat('% a.a.');
        this.chartOptions = {
            chart: {
                type: 'spline'
            },
            title: {
                text: actual,
                style: {
                    color: "1F55AD"
                }
            },
            xAxis: {
                categories: listCategories,
                title: {
                    text: 'Períodos coletados'
                }
            },
            yAxis: {
                title: {
                    text: 'Valores'
                }
            },
            series: [{
                    name: 'SELIC',
                    data: listData
                }]
        };
    };
    SelicPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__selic_selic_modal__["a" /* SelicModalPage */]);
        modal.present();
    };
    SelicPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\selic\selic.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-img class="img-header" width="24" height="24" src="assets/imgs/icone-selic.svg"></ion-img>\n\n                <h4 class="content-title">Taxa Selic</h4>\n\n                <a href="#" (click)="openModal()">O que é Selic?</a>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <chart [options]="chartOptions" style="display: block" type="chart"> </chart>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\selic\selic.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_3__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__shared_req_service__["a" /* RequestService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* ModalController */]])
    ], SelicPage);
    return SelicPage;
}());

//# sourceMappingURL=selic.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelicModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SelicModalPage = /** @class */ (function () {
    function SelicModalPage(viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.text = '';
        this.text = 'Define-se Taxa Selic como a taxa média ajustada dos financiamentos diários apurados no Sistema Especial de Liquidação e de Custódia (Selic) para títulos federais. Para fins de cálculo da taxa, são considerados os financiamentos diários relativos às operações registradas e liquidadas no próprio Selic e em sistemas operados por câmaras ou prestadores de serviços de compensação e de liquidação (art. 1° da Circular n° 2.900, de 24 de junho de 1999, com a alteração introduzida pelo art. 1° da Circular n° 3.119, de 18 de abril de 2002).';
    }
    SelicModalPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    SelicModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\selic\selic-modal.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <h6>O que é Selic?</h6>\n\n    </ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="closeModal()">\n\n        <ion-icon item-right name="ios-close-outline"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n        <ion-img class="img-header" width="48" height="48" src="assets/imgs/icone-selic.svg"></ion-img>\n\n        <h4>O que é Selic?</h4>\n\n      </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n      \n\n      <p>\n\n        {{text}}\n\n      </p>\n\n\n\n      </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n\n\n        <button ion-button full (click)="closeModal()">OK</button>\n\n      \n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\selic\selic-modal.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */]])
    ], SelicModalPage);
    return SelicModalPage;
}());

//# sourceMappingURL=selic-modal.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IpcaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__select_rate_select_rate__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ipca_ipca_modal__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var IpcaPage = /** @class */ (function () {
    function IpcaPage(navCtrl, dialogs, globals, plt, rqService, modalCtrl) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.plt = plt;
        this.rqService = rqService;
        this.modalCtrl = modalCtrl;
    }
    IpcaPage.prototype.ionViewDidLoad = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getIpcaRateList();
        }
    };
    IpcaPage.prototype.getIpcaRateList = function () {
        var _this = this;
        this.rqService.getLastIpcaRate().then(function (data) {
            _this.ipcaRateList = data;
            _this.buildChart(_this.ipcaRateList);
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS)
                .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__select_rate_select_rate__["a" /* SelectRatePage */]); });
        });
    };
    IpcaPage.prototype.buildChart = function (listRate) {
        var listData = listRate.map(function (a) { return Number(a.valor); });
        var listCategories = listRate.map(function (a) { return a.data; });
        var actual = 'Taxa (IPCA) Comercializáveis atual '.concat(listData[listData.length - 1]).concat(' %');
        this.chartOptions = {
            chart: {
                type: 'spline'
            },
            title: {
                text: actual,
                style: {
                    color: "1F55AD"
                }
            },
            xAxis: {
                categories: listCategories,
                title: {
                    text: 'Períodos coletados'
                }
            },
            yAxis: {
                title: {
                    text: 'Valores'
                }
            },
            series: [{
                    name: 'IPCA',
                    data: listData
                }]
        };
    };
    IpcaPage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__ipca_ipca_modal__["a" /* IpcaModalPage */]);
        modal.present();
    };
    IpcaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ipca\ipca.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-img class="img-header" width="24" height="24" src="assets/imgs/icone-ipca.svg"></ion-img>\n\n                <h4 class="content-title">IPCA</h4>\n\n                <a href="#" (click)="openModal()">O que é IPCA?</a>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <chart [options]="chartOptions" style="display: block" type="chart"> </chart>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ipca\ipca.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_3__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__shared_req_service__["a" /* RequestService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* ModalController */]])
    ], IpcaPage);
    return IpcaPage;
}());

//# sourceMappingURL=ipca.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IpcaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IpcaModalPage = /** @class */ (function () {
    function IpcaModalPage(viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.text = '';
        this.text = 'Índice nacional de preços ao consumidor-Amplo (IPCA) - Comercializáveis. ' + 'Conceito: Indicadores que mensuram a variação de preços (sob a ótica do consumidor). Os principais indicadores coletados são: IPC-Fipe IPCA IPCA-15 INPC ICV IPCA-E.';
    }
    IpcaModalPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    IpcaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ipca\ipca-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-title>\n\n        <h6>O que é IPCA?</h6>\n\n      </ion-title>\n\n      <ion-buttons end>\n\n        <button ion-button icon-only (click)="closeModal()">\n\n          <ion-icon item-right name="ios-close-outline"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n<ion-content>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n        <ion-img class="img-header" width="48" height="48" src="assets/imgs/icone-ipca.svg"></ion-img>\n\n        <h4>O que é IPCA?</h4>\n\n      </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n      \n\n      <p>\n\n        {{text}}\n\n      </p>\n\n\n\n      </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n\n\n      <ion-col col-12>\n\n\n\n        <button ion-button full (click)="closeModal()">OK</button>\n\n      \n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ipca\ipca-modal.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */]])
    ], IpcaModalPage);
    return IpcaModalPage;
}());

//# sourceMappingURL=ipca-modal.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OmbudsmanContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_req_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OmbudsmanContactPage = /** @class */ (function () {
    function OmbudsmanContactPage(dialogs, globals, plt, rqService) {
        this.dialogs = dialogs;
        this.globals = globals;
        this.plt = plt;
        this.rqService = rqService;
        this.ombudsmanContactList = [];
    }
    OmbudsmanContactPage.prototype.getOmbudsmanName = function (ombudsmanName) {
        var _this = this;
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.rqService.getOmbudsmanContact(ombudsmanName.trim()).then(function (data) {
                _this.ombudsmanContactList = data;
            }).catch(function (error) {
                _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS);
            });
        }
    };
    OmbudsmanContactPage.prototype.search = function (ev) {
        var val = ev.target.value;
        this.ombudsmanContactList = [];
        if (val) {
            this.getOmbudsmanName(val.toUpperCase());
        }
    };
    OmbudsmanContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ombudsman-contact\ombudsman-contact.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n    <ion-searchbar (keyup.enter)="search($event)" placeholder="Digite o nome da instituição..."></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row *ngIf="(ombudsmanContactList.length == 0)">\n\n            <ion-col col-12>\n\n                <h6 class="content-title" text-wrap>INSIRA O NOME DA INSTITUIÇÃO E PESQUISE PARA OBTER O(S) CONTATO(S) DE OUVIDORIA(S).</h6>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <ion-list>\n\n                    <ion-item text-wrap *ngFor="let ombudsmanContact of ombudsmanContactList">\n\n                        <h3>Nome: {{ombudsmanContact.Nome}}</h3>\n\n                        <h4>Telefone: {{ombudsmanContact.Telefone}}</h4>\n\n                        <p><strong>Ouvidor:</strong>&nbsp;{{ombudsmanContact.Ouvidor}}</p>\n\n                        <p><strong>CNPJ:</strong>&nbsp;{{ombudsmanContact.CNPJ}}</p>\n\n                        <p><strong>Website:</strong>&nbsp;{{ombudsmanContact.WebSite}}</p>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\ombudsman-contact\ombudsman-contact.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__shared_req_service__["a" /* RequestService */]])
    ], OmbudsmanContactPage);
    return OmbudsmanContactPage;
}());

//# sourceMappingURL=ombudsman-contact.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EletronicUnitsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__eletronic_unit_post_eletronic_unit_post__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_globals__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EletronicUnitsPage = /** @class */ (function () {
    function EletronicUnitsPage(navCtrl, dialogs, globals, plt, rqService) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.plt = plt;
        this.rqService = rqService;
    }
    EletronicUnitsPage.prototype.ionViewDidLoad = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getEletronicUnits(this.globals.ELETRONIC_UNIT_SEARCH);
        }
    };
    EletronicUnitsPage.prototype.getEletronicUnits = function (params) {
        var _this = this;
        this.rqService.getEletronicUnits(params.institution, params.address, params.city).then(function (data) {
            _this.eletronicUnitsList = data;
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS)
                .then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__eletronic_unit_post_eletronic_unit_post__["a" /* EletronicUnitPostPage */]); });
        });
    };
    EletronicUnitsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\eletronic-units\eletronic-units.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <h4 class="content-title">POSTOS DE ATENDIMENTO ELETRÔNICO</h4>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n        \n\n            <ion-col col-12>\n\n                <ion-list>\n\n                    <ion-item text-wrap *ngFor="let eleUnit of eletronicUnitsList">\n\n                        <h3>{{eleUnit.NomeIf}}</h3>\n\n                        <h3>{{eleUnit.NomePosto}}</h3>\n\n                        <p>{{eleUnit.Endereco}}, {{eleUnit.Numero}} {{eleUnit.Bairro}}</p>\n\n                        <p>{{eleUnit.Municipio}}, {{eleUnit.UF}}</p>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\eletronic-units\eletronic-units.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_dialogs__["a" /* Dialogs */], __WEBPACK_IMPORTED_MODULE_5__shared_globals__["a" /* Globals */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__shared_req_service__["a" /* RequestService */]])
    ], EletronicUnitsPage);
    return EletronicUnitsPage;
}());

//# sourceMappingURL=eletronic-units.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NextPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_geolocation__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_req_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NextPage = /** @class */ (function () {
    function NextPage(geolocation, http, platform, dialogs, loadingController, rqService) {
        this.geolocation = geolocation;
        this.http = http;
        this.platform = platform;
        this.dialogs = dialogs;
        this.loadingController = loadingController;
        this.rqService = rqService;
        this.loading = this.loadingController.create({ content: 'Obtendo localização...' });
    }
    NextPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.obterCoords();
        });
    };
    NextPage.prototype.obterCoords = function () {
        var _this = this;
        this.loading.present();
        var options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };
        this.geolocation.getCurrentPosition(options).then(function (resp) {
            _this.obterEnderecoPorCoord(resp.coords.latitude, resp.coords.longitude);
        }, function (err) {
            _this.loading.dismissAll();
            _this.dialogs.alert('Ocorreu uma falha ao obter sua localização.', 'Ops')
                .then(function () { return console.log('Dialog dismissed'); })
                .catch(function (e) { return console.log('Error displaying dialog', e); });
        });
    };
    NextPage.prototype.obterEnderecoPorCoord = function (lat, long) {
        var _this = this;
        this.http.get('https://maps.googleapis.com/maps/api/geocode/json', { 'latlng': lat + ',' + long, 'key': 'AIzaSyAI9Ck8i6cf3rhmV0xf79HwaO20jLNnG6I' }, {})
            .then(function (data) {
            var jsondata = JSON.parse(data.data);
            var resolved = Array();
            resolved = jsondata.results;
            _this.loading.dismissAll();
            _this.getEletronicUnits(resolved);
        })
            .catch(function (error) {
            _this.loading.dismissAll();
            _this.dialogs.alert('Ocorreu uma falha ao obter sua localização.', 'Ops')
                .then(function () { return console.log('Dialog dismissed'); })
                .catch(function (e) { return console.log('Error displaying dialog', e); });
        });
    };
    NextPage.prototype.getEletronicUnits = function (data) {
        var _this = this;
        var rua = data[0].address_components[1].long_name;
        //let bairro = data[0].address_components[2].long_name;
        var cidade = data[0].address_components[3].long_name;
        //let estado = data[0].address_components[5].long_name;
        this.endereco_completo = data[0].formatted_address;
        this.rqService.getEletronicUnitsByAddressAndCity(rua, cidade).then(function (data) {
            _this.endereco_list = data;
        }).catch(function (error) {
            console.log('error == ' + JSON.stringify(error));
            _this.dialogs.alert('Falha ao obter os postos eletrônicos, por favor tente outra forma de pesquisa.', 'Ops');
        });
    };
    NextPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\next\next.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>CAIXAS ELETRÔNICOS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <ion-card>\n\n        <ion-card-header>\n\n            ENDEREÇO ATUAL\n\n        </ion-card-header>\n\n        <ion-card-content>\n\n            {{endereco_completo}}\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-list>\n\n        <ion-card text-wrap *ngFor="let end of endereco_list">\n\n            <ion-card-header>\n\n                {{end.NomeIf}}\n\n            </ion-card-header>\n\n            <ion-card-content>\n\n                {{end.TipoPosto}} - {{end.NomePosto}} {{end.Endereco}} {{end.Numero}} - {{end.Bairro}} - {{end.Municipio}}\n\n            </ion-card-content>\n\n        </ion-card>\n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\next\next.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__shared_req_service__["a" /* RequestService */]])
    ], NextPage);
    return NextPage;
}());

//# sourceMappingURL=next.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(364);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_highcharts__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_highcharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_highcharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_Highcharts__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_Highcharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_Highcharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_moment__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_shared_req_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_consolidated_groups_consolidated_groups__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_rates_institutions_rate_institution__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_institutions_institution__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_selic_selic__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_ipca_ipca__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_select_rate_select_rate__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_ombudsman_contact_ombudsman_contact__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_eletronic_unit_post_eletronic_unit_post__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_eletronic_units_eletronic_units__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_next_next__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_http__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_selic_selic_modal__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_ipca_ipca_modal__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_rates_institutions_rate_institution__["a" /* RateInstitutionPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_institutions_institution__["a" /* InstitutionPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_selic_selic__["a" /* SelicPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_ipca_ipca__["a" /* IpcaPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_select_rate_select_rate__["a" /* SelectRatePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_ombudsman_contact_ombudsman_contact__["a" /* OmbudsmanContactPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_eletronic_unit_post_eletronic_unit_post__["a" /* EletronicUnitPostPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_eletronic_units_eletronic_units__["a" /* EletronicUnitsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_selic_selic_modal__["a" /* SelicModalPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_ipca_ipca_modal__["a" /* IpcaModalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_next_next__["a" /* NextPage */],
                __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__["a" /* PopoverComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5_ngx_moment__["a" /* MomentModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {
                    tabsPlacement: 'top',
                    platforms: {
                        ios: {
                            tabsPlacement: 'bottom',
                        }
                    }
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_3_angular2_highcharts__["ChartModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4_Highcharts__)
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_consolidated_groups_consolidated_groups__["a" /* ConsolidatedGroupsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_rates_institutions_rate_institution__["a" /* RateInstitutionPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_institutions_institution__["a" /* InstitutionPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_selic_selic__["a" /* SelicPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_ipca_ipca__["a" /* IpcaPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_select_rate_select_rate__["a" /* SelectRatePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_ombudsman_contact_ombudsman_contact__["a" /* OmbudsmanContactPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_eletronic_unit_post_eletronic_unit_post__["a" /* EletronicUnitPostPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_eletronic_units_eletronic_units__["a" /* EletronicUnitsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_selic_selic_modal__["a" /* SelicModalPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_ipca_ipca_modal__["a" /* IpcaModalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_next_next__["a" /* NextPage */],
                __WEBPACK_IMPORTED_MODULE_24__components_popover_popover__["a" /* PopoverComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_dialogs__["a" /* Dialogs */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_9__pages_shared_globals__["a" /* Globals */],
                __WEBPACK_IMPORTED_MODULE_7__pages_shared_req_service__["a" /* RequestService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 417:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 203,
	"./af.js": 203,
	"./ar": 204,
	"./ar-dz": 205,
	"./ar-dz.js": 205,
	"./ar-kw": 206,
	"./ar-kw.js": 206,
	"./ar-ly": 207,
	"./ar-ly.js": 207,
	"./ar-ma": 208,
	"./ar-ma.js": 208,
	"./ar-sa": 209,
	"./ar-sa.js": 209,
	"./ar-tn": 210,
	"./ar-tn.js": 210,
	"./ar.js": 204,
	"./az": 211,
	"./az.js": 211,
	"./be": 212,
	"./be.js": 212,
	"./bg": 213,
	"./bg.js": 213,
	"./bm": 214,
	"./bm.js": 214,
	"./bn": 215,
	"./bn.js": 215,
	"./bo": 216,
	"./bo.js": 216,
	"./br": 217,
	"./br.js": 217,
	"./bs": 218,
	"./bs.js": 218,
	"./ca": 219,
	"./ca.js": 219,
	"./cs": 220,
	"./cs.js": 220,
	"./cv": 221,
	"./cv.js": 221,
	"./cy": 222,
	"./cy.js": 222,
	"./da": 223,
	"./da.js": 223,
	"./de": 224,
	"./de-at": 225,
	"./de-at.js": 225,
	"./de-ch": 226,
	"./de-ch.js": 226,
	"./de.js": 224,
	"./dv": 227,
	"./dv.js": 227,
	"./el": 228,
	"./el.js": 228,
	"./en-au": 229,
	"./en-au.js": 229,
	"./en-ca": 230,
	"./en-ca.js": 230,
	"./en-gb": 231,
	"./en-gb.js": 231,
	"./en-ie": 232,
	"./en-ie.js": 232,
	"./en-il": 233,
	"./en-il.js": 233,
	"./en-nz": 234,
	"./en-nz.js": 234,
	"./eo": 235,
	"./eo.js": 235,
	"./es": 236,
	"./es-do": 237,
	"./es-do.js": 237,
	"./es-us": 238,
	"./es-us.js": 238,
	"./es.js": 236,
	"./et": 239,
	"./et.js": 239,
	"./eu": 240,
	"./eu.js": 240,
	"./fa": 241,
	"./fa.js": 241,
	"./fi": 242,
	"./fi.js": 242,
	"./fo": 243,
	"./fo.js": 243,
	"./fr": 244,
	"./fr-ca": 245,
	"./fr-ca.js": 245,
	"./fr-ch": 246,
	"./fr-ch.js": 246,
	"./fr.js": 244,
	"./fy": 247,
	"./fy.js": 247,
	"./gd": 248,
	"./gd.js": 248,
	"./gl": 249,
	"./gl.js": 249,
	"./gom-latn": 250,
	"./gom-latn.js": 250,
	"./gu": 251,
	"./gu.js": 251,
	"./he": 252,
	"./he.js": 252,
	"./hi": 253,
	"./hi.js": 253,
	"./hr": 254,
	"./hr.js": 254,
	"./hu": 255,
	"./hu.js": 255,
	"./hy-am": 256,
	"./hy-am.js": 256,
	"./id": 257,
	"./id.js": 257,
	"./is": 258,
	"./is.js": 258,
	"./it": 259,
	"./it.js": 259,
	"./ja": 260,
	"./ja.js": 260,
	"./jv": 261,
	"./jv.js": 261,
	"./ka": 262,
	"./ka.js": 262,
	"./kk": 263,
	"./kk.js": 263,
	"./km": 264,
	"./km.js": 264,
	"./kn": 265,
	"./kn.js": 265,
	"./ko": 266,
	"./ko.js": 266,
	"./ky": 267,
	"./ky.js": 267,
	"./lb": 268,
	"./lb.js": 268,
	"./lo": 269,
	"./lo.js": 269,
	"./lt": 270,
	"./lt.js": 270,
	"./lv": 271,
	"./lv.js": 271,
	"./me": 272,
	"./me.js": 272,
	"./mi": 273,
	"./mi.js": 273,
	"./mk": 274,
	"./mk.js": 274,
	"./ml": 275,
	"./ml.js": 275,
	"./mn": 276,
	"./mn.js": 276,
	"./mr": 277,
	"./mr.js": 277,
	"./ms": 278,
	"./ms-my": 279,
	"./ms-my.js": 279,
	"./ms.js": 278,
	"./mt": 280,
	"./mt.js": 280,
	"./my": 281,
	"./my.js": 281,
	"./nb": 282,
	"./nb.js": 282,
	"./ne": 283,
	"./ne.js": 283,
	"./nl": 284,
	"./nl-be": 285,
	"./nl-be.js": 285,
	"./nl.js": 284,
	"./nn": 286,
	"./nn.js": 286,
	"./pa-in": 287,
	"./pa-in.js": 287,
	"./pl": 288,
	"./pl.js": 288,
	"./pt": 289,
	"./pt-br": 290,
	"./pt-br.js": 290,
	"./pt.js": 289,
	"./ro": 291,
	"./ro.js": 291,
	"./ru": 292,
	"./ru.js": 292,
	"./sd": 293,
	"./sd.js": 293,
	"./se": 294,
	"./se.js": 294,
	"./si": 295,
	"./si.js": 295,
	"./sk": 296,
	"./sk.js": 296,
	"./sl": 297,
	"./sl.js": 297,
	"./sq": 298,
	"./sq.js": 298,
	"./sr": 299,
	"./sr-cyrl": 300,
	"./sr-cyrl.js": 300,
	"./sr.js": 299,
	"./ss": 301,
	"./ss.js": 301,
	"./sv": 302,
	"./sv.js": 302,
	"./sw": 303,
	"./sw.js": 303,
	"./ta": 304,
	"./ta.js": 304,
	"./te": 305,
	"./te.js": 305,
	"./tet": 306,
	"./tet.js": 306,
	"./tg": 307,
	"./tg.js": 307,
	"./th": 308,
	"./th.js": 308,
	"./tl-ph": 309,
	"./tl-ph.js": 309,
	"./tlh": 310,
	"./tlh.js": 310,
	"./tr": 311,
	"./tr.js": 311,
	"./tzl": 312,
	"./tzl.js": 312,
	"./tzm": 313,
	"./tzm-latn": 314,
	"./tzm-latn.js": 314,
	"./tzm.js": 313,
	"./ug-cn": 315,
	"./ug-cn.js": 315,
	"./uk": 316,
	"./uk.js": 316,
	"./ur": 317,
	"./ur.js": 317,
	"./uz": 318,
	"./uz-latn": 319,
	"./uz-latn.js": 319,
	"./uz.js": 318,
	"./vi": 320,
	"./vi.js": 320,
	"./x-pseudo": 321,
	"./x-pseudo.js": 321,
	"./yo": 322,
	"./yo.js": 322,
	"./zh-cn": 323,
	"./zh-cn.js": 323,
	"./zh-hk": 324,
	"./zh-hk.js": 324,
	"./zh-tw": 325,
	"./zh-tw.js": 325
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 417;

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            _this.initializeApp();
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            //statusBar.styleDefault();
            //splashScreen.hide();
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.splashScreen.hide();
            _this.statusBar.styleDefault();
            // let status bar overlay webview
            _this.statusBar.overlaysWebView(false);
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\app\app.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsolidatedGroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__institutions_institution__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_globals__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_popover_popover__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_req_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ConsolidatedGroupsPage = /** @class */ (function () {
    function ConsolidatedGroupsPage(navCtrl, dialogs, globals, plt, rqService, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.dialogs = dialogs;
        this.globals = globals;
        this.plt = plt;
        this.rqService = rqService;
        this.popoverCtrl = popoverCtrl;
    }
    ConsolidatedGroupsPage.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_popover_popover__["a" /* PopoverComponent */]);
        popover.present({
            ev: myEvent
        });
    };
    ConsolidatedGroupsPage.prototype.ionViewDidLoad = function () {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getAllItems();
        }
    };
    ConsolidatedGroupsPage.prototype.getAllItems = function () {
        var _this = this;
        this.rqService.getAllConsolidatedGroups().then(function (data) {
            data.forEach(function (item) { item.Nome = item.Nome.toUpperCase(); });
            _this.groups_consolidated = data;
            _this.temporary_groups = data;
        }).catch(function (error) {
            _this.dialogs.alert(_this.globals.FAIL_REQUEST_MESSAGE, _this.globals.OPS);
        });
    };
    ConsolidatedGroupsPage.prototype.itemTapped = function (group) {
        this.globals.CONSOLIDATED_GROUP_SELECTED = group;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__institutions_institution__["a" /* InstitutionPage */], {});
    };
    ConsolidatedGroupsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.groups_consolidated = this.temporary_groups;
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.groups_consolidated = this.groups_consolidated.filter(function (item) {
                return (item.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ConsolidatedGroupsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\consolidated-groups\consolidated-groups.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      <h6>TARIFAS</h6>\n\n    </ion-title>\n\n    <ion-buttons end class="botao-pessoa">\n\n      <button ion-button icon-only (click)="presentPopover($event)">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar (ionInput)="getItems($event)" placeholder="Pesquisar grupos..."></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <h4 class="content-title">GRUPOS CONSOLIDADOS</h4>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n              <ion-list>\n\n                <button text-wrap ion-item *ngFor="let cg of groups_consolidated" (click)="itemTapped(cg)">\n\n                  {{ cg.Nome }}\n\n                </button>\n\n              </ion-list>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n  </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\consolidated-groups\consolidated-groups.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_dialogs__["a" /* Dialogs */],
            __WEBPACK_IMPORTED_MODULE_4__shared_globals__["a" /* Globals */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__shared_req_service__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* PopoverController */]])
    ], ConsolidatedGroupsPage);
    return ConsolidatedGroupsPage;
}());

//# sourceMappingURL=consolidated-groups.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectRatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__selic_selic__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ipca_ipca__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SelectRatePage = /** @class */ (function () {
    function SelectRatePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SelectRatePage.prototype.onButtonClick = function (rate) {
        switch (rate) {
            case 'selic': {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__selic_selic__["a" /* SelicPage */], {});
                break;
            }
            case 'ipca': {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__ipca_ipca__["a" /* IpcaPage */], {});
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    };
    SelectRatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\Pablo\Downloads\Tarifas\src\pages\select-rate\select-rate.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>\n\n            <h6>TARIFAS</h6>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid>\n\n\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <h4 class="content-title">SELECIONE UMA TAXA</h4>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col col-6>\n\n                <button class="big-rate-button" (click)="onButtonClick(\'selic\')" block>\n\n                    <ion-img width="64" height="64" src="assets/imgs/icone-selic.svg"></ion-img>\n\n                    Selic\n\n                </button>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n                <button class="big-rate-button" (click)="onButtonClick(\'ipca\')" block>\n\n                    <ion-img width="64" height="64" src="assets/imgs/icone-ipca.svg"></ion-img>\n\n                    IPCA\n\n                </button>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\Pablo\Downloads\Tarifas\src\pages\select-rate\select-rate.html"*/ }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], SelectRatePage);
    return SelectRatePage;
}());

//# sourceMappingURL=select-rate.js.map

/***/ })

},[341]);
//# sourceMappingURL=main.js.map