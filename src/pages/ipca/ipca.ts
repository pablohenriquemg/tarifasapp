import { Component } from '@angular/core';
import { NavController, Platform, ModalController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { Globals } from '../shared/globals';
import { RequestService } from '../shared/req-service';
import { SelectRatePage } from '../select-rate/select-rate';
import { IpcaModalPage } from '../ipca/ipca-modal';

@Component({ templateUrl: 'ipca.html' })
export class IpcaPage {

  ipcaRateList: Array<RateModel>;
  chartOptions: any;

  constructor(private navCtrl: NavController,
    private dialogs: Dialogs,
    private globals: Globals,
    private plt: Platform,
    private rqService: RequestService,
    private modalCtrl: ModalController) { }

  ionViewDidLoad() {
    if (this.plt.is('ios') || this.plt.is('android')) {
      this.getIpcaRateList();
    }
  }

  getIpcaRateList() {
    this.rqService.getLastIpcaRate().then((data) => {
      this.ipcaRateList = data;
      this.buildChart(this.ipcaRateList);
    }).catch(error => {
      this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
        .then(() => this.navCtrl.push(SelectRatePage));
    });
  }

  buildChart(listRate) {

    let listData = listRate.map(a => Number(a.valor));
    let listCategories = listRate.map(a => a.data);

    let actual = 'Taxa (IPCA) Comercializáveis atual '.concat(listData[listData.length - 1]).concat(' %');

    this.chartOptions = {
      chart: {
        type: 'spline'
      },
      title: {
        text: actual,
        style: {
          color: "1F55AD"
        }
      },
      xAxis: {
        categories: listCategories,
        title: {
          text: 'Períodos coletados'
        }
      },
      yAxis: {
        title: {
          text: 'Valores'
        }
      },
      series: [{
        name: 'IPCA',
        data: listData
      }]

    }

  }

  openModal() {
    let modal = this.modalCtrl.create(IpcaModalPage);
    modal.present();
  }

}