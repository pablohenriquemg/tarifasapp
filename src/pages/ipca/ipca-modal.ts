import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({ templateUrl: 'ipca-modal.html' })
export class IpcaModalPage {

    text = '';

    constructor(
        public viewCtrl: ViewController
    ) {
        this.text = 'Índice nacional de preços ao consumidor-Amplo (IPCA) - Comercializáveis. ' + 'Conceito: Indicadores que mensuram a variação de preços (sob a ótica do consumidor). Os principais indicadores coletados são: IPC-Fipe IPCA IPCA-15 INPC ICV IPCA-E.'
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }
}