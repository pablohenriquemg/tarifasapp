import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { HTTP } from '@ionic-native/http';
import { Platform } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { LoadingController } from 'ionic-angular';
import { RequestService } from '../shared/req-service';

@Component({ templateUrl: 'next.html' })
export class NextPage {

    endereco_completo: string;
    endereco_list: Array<EletronicUnitModel>;
    loading = this.loadingController.create({ content: 'Obtendo localização...' });

    constructor(private geolocation: Geolocation,
        private http: HTTP, private platform: Platform,
        private dialogs: Dialogs,
        private loadingController: LoadingController,
        private rqService: RequestService) { }

    ionViewDidLoad() {
        this.platform.ready().then(() => {
            this.obterCoords()
        })
    }

    obterCoords() {
        this.loading.present();
        var options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        this.geolocation.getCurrentPosition(options).then((resp) => {
            this.obterEnderecoPorCoord(resp.coords.latitude, resp.coords.longitude)
        }, (err) => {
            this.loading.dismissAll();
            this.dialogs.alert('Ocorreu uma falha ao obter sua localização.', 'Ops')
                .then(() => console.log('Dialog dismissed'))
                .catch(e => console.log('Error displaying dialog', e));
        });
    }

    obterEnderecoPorCoord(lat: number, long: number) {
        this.http.get('https://maps.googleapis.com/maps/api/geocode/json',
            { 'latlng': lat + ',' + long, 'key': 'AIzaSyAI9Ck8i6cf3rhmV0xf79HwaO20jLNnG6I' }, {})
            .then(data => {
                let jsondata = JSON.parse(data.data);
                let resolved = Array<AddressComponent>();
                resolved = jsondata.results;
                this.loading.dismissAll();
                this.getEletronicUnits(resolved)
            })
            .catch(error => {
                this.loading.dismissAll();
                this.dialogs.alert('Ocorreu uma falha ao obter sua localização.', 'Ops')
                    .then(() => console.log('Dialog dismissed'))
                    .catch(e => console.log('Error displaying dialog', e));
            });
    }

    getEletronicUnits(data: Array<AddressComponent>) {
        let rua = data[0].address_components[1].long_name;
        //let bairro = data[0].address_components[2].long_name;
        let cidade = data[0].address_components[3].long_name;
        //let estado = data[0].address_components[5].long_name;

        this.endereco_completo = data[0].formatted_address;

        this.rqService.getEletronicUnitsByAddressAndCity(rua, cidade).then((data) => {
            this.endereco_list = data;
        }).catch(error => {
            console.log('error == ' + JSON.stringify(error))
            this.dialogs.alert('Falha ao obter os postos eletrônicos, por favor tente outra forma de pesquisa.', 'Ops');
        });
    }
}
