import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { RateInstitutionPage } from '../rates-institutions/rate-institution';
import { Globals } from '../shared/globals';
import { RequestService } from '../shared/req-service';
import { ConsolidatedGroupsPage } from '../consolidated-groups/consolidated-groups';

@Component({ templateUrl: 'institution.html' })
export class InstitutionPage {

    institutions: Array<InstitutionModel>;
    temporary_institutions: Array<InstitutionModel>;

    constructor(private navCtrl: NavController,
        private dialogs: Dialogs, private globals: Globals,
        private rqService: RequestService) { }

    ionViewDidLoad() {
        this.getAllItems();
    }

    getAllItems() {
        let groupCode = this.globals.CONSOLIDATED_GROUP_SELECTED.Codigo;

        this.rqService.getInstitutionsByConsolidatedGroup(groupCode).then((data) => {
            this.institutions = data;
            this.temporary_institutions = data;
        }).catch(error => {
            this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
                .then(() => this.navCtrl.push(ConsolidatedGroupsPage));
        });

    }

    itemTapped(institution) {
        this.globals.INSTITUTION_SELECTED = institution;
        this.navCtrl.push(RateInstitutionPage, {});
    }

    getItems(ev) {

        // Reset items back to all of the items
        this.institutions = this.temporary_institutions;

        // set val to the value of the ev target
        var val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.institutions = this.institutions.filter((item) => {
                return (item.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

}