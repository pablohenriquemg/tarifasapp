import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { RequestService } from '../shared/req-service';
import { Dialogs } from '@ionic-native/dialogs';
import { EletronicUnitPostPage } from '../eletronic-unit-post/eletronic-unit-post';
import { Globals } from '../shared/globals';

@Component({ templateUrl: 'eletronic-units.html' })
export class EletronicUnitsPage {

    eletronicUnitsList: Array<EletronicUnitModel>;

    constructor(private navCtrl: NavController,
        private dialogs: Dialogs, private globals: Globals,
        private plt: Platform, private rqService: RequestService) { }

    ionViewDidLoad() {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getEletronicUnits(this.globals.ELETRONIC_UNIT_SEARCH);
        }
    }

    getEletronicUnits(params) {
        this.rqService.getEletronicUnits(params.institution, params.address, params.city).then((data) => {
            this.eletronicUnitsList = data;
        }).catch(error => {
            this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
                .then(() => this.navCtrl.push(EletronicUnitPostPage));
        });
    }

}