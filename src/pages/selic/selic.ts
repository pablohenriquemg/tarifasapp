import { Component } from '@angular/core';
import { NavController, Platform, ModalController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { Globals } from '../shared/globals';
import { RequestService } from '../shared/req-service';
import { SelectRatePage } from '../select-rate/select-rate';
import { SelicModalPage } from '../selic/selic-modal';

@Component({ templateUrl: 'selic.html' })
export class SelicPage {

    selicRateList: Array<RateModel>;
    chartOptions: any;

    constructor(private navCtrl: NavController,
        private dialogs: Dialogs,
        private globals: Globals,
        private plt: Platform,
        private rqService: RequestService,
        private modalCtrl: ModalController) { }

    ionViewDidLoad() {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getSelicRateList();
        }
    }

    getSelicRateList() {
        this.rqService.getLastSelicRate().then((data) => {
            this.selicRateList = data;
            this.buildChart(this.getSampleResult(this.selicRateList));
        }).catch(error => {
            this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
                .then(() => this.navCtrl.push(SelectRatePage));
        });
    }

    getSampleResult(arr: Array<RateModel>) {
        let arrFiltered = Array<RateModel>();
        arr = arr.reverse();
        for (var i = 0; i < arr.length; i += 30) {
            arrFiltered.push(arr[i])
        }
        return arrFiltered.reverse();
    }

    buildChart(listRate) {

        let listData = listRate.map(a => parseFloat(a.valor));
        let listCategories = listRate.map(a => a.data);
        let actual = 'Taxa atual de '.concat(listData[listData.length - 1]).concat('% a.a.');

        this.chartOptions = {
            chart: {
                type: 'spline'
            },
            title: {
                text: actual,
                style: {
                  color: "1F55AD"
                }
            },
            xAxis: {
                categories: listCategories,
                title: {
                    text: 'Períodos coletados'
                }
            },
            yAxis: {
                title: {
                    text: 'Valores'
                }
            },
            series: [{
                name: 'SELIC',
                data: listData
            }]

        }
    }

    openModal() {
        let modal = this.modalCtrl.create(SelicModalPage);
        modal.present();
    }

}