import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({ templateUrl: 'selic-modal.html' })
export class SelicModalPage {

    text = '';

    constructor(
        public viewCtrl: ViewController
    ) {
        this.text = 'Define-se Taxa Selic como a taxa média ajustada dos financiamentos diários apurados no Sistema Especial de Liquidação e de Custódia (Selic) para títulos federais. Para fins de cálculo da taxa, são considerados os financiamentos diários relativos às operações registradas e liquidadas no próprio Selic e em sistemas operados por câmaras ou prestadores de serviços de compensação e de liquidação (art. 1° da Circular n° 2.900, de 24 de junho de 1999, com a alteração introduzida pelo art. 1° da Circular n° 3.119, de 18 de abril de 2002).'
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }
}