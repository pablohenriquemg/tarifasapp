import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { EletronicUnitsPage } from '../eletronic-units/eletronic-units';
import { Dialogs } from '@ionic-native/dialogs';
import { Globals } from '../shared/globals';
import { NextPage } from '../next/next';


@Component({ templateUrl: 'eletronic-unit-post.html' })
export class EletronicUnitPostPage {

    institutionName: string = '';
    address: string = '';
    city: string = '';

    constructor(private navCtrl: NavController,
        private plt: Platform, private dialogs: Dialogs,
        private globals: Globals) { }

    startSearch() {
        if (this.plt.is('ios') || this.plt.is('android')) {
            if (!this.isNullOrEmpty(this.institutionName) || !this.isNullOrEmpty(this.address) || !this.isNullOrEmpty(this.city)) {
                this.globals.ELETRONIC_UNIT_SEARCH = { 'institution': this.institutionName, 'address': this.address, 'city': this.city };
                this.navCtrl.push(EletronicUnitsPage, {});
            } else {
                this.dialogs.alert(this.globals.FILL_ONLY_ONE_FIELD, this.globals.OPS)
            }
        }
    }

    isNullOrEmpty = function (value) {
        return (!value || value == undefined || value == "" || value.length == 0);
    }

    searchNear() {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.navCtrl.push(NextPage, {});
        }
    }

}