import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { InstitutionPage } from '../institutions/institution';
import { Globals } from '../shared/globals';
import { PopoverComponent } from '../../components/popover/popover';
import { RequestService } from '../shared/req-service';
import { PopoverController } from 'ionic-angular';

@Component({ templateUrl: 'consolidated-groups.html' })
export class ConsolidatedGroupsPage {

    groups_consolidated: Array<ConsolidatedGroupModel>;
    temporary_groups: Array<ConsolidatedGroupModel>;

    constructor(private navCtrl: NavController, private dialogs: Dialogs,
        private globals: Globals, private plt: Platform,
        private rqService: RequestService, private popoverCtrl: PopoverController) {
    }

    presentPopover(myEvent) {
        let popover = this.popoverCtrl.create(PopoverComponent);
        popover.present({
            ev: myEvent
        });
    }

    ionViewDidLoad() {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.getAllItems();
        }
    }

    getAllItems() {
        this.rqService.getAllConsolidatedGroups().then((data) => {
            data.forEach((item) => {item.Nome = item.Nome.toUpperCase();})
            this.groups_consolidated = data;
            this.temporary_groups = data;
        }).catch(error => {
            this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
        });
    }

    itemTapped(group) {
        this.globals.CONSOLIDATED_GROUP_SELECTED = group;
        this.navCtrl.push(InstitutionPage, {});
    }

    getItems(ev) {
        // Reset items back to all of the items
        this.groups_consolidated = this.temporary_groups;
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.groups_consolidated = this.groups_consolidated.filter((item) => {
                return (item.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

}