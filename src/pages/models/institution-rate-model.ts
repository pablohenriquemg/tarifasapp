class InstitutionRateModel {

    CodigoServico: string;
    Servico: string;
    Unidade: string;
    DataVigencia: string;
    ValorMaximo: number;
    Periodicidade: string;

    constructor(codigoServico: string, servico: string, unidade: string, dataVigencia: string,
        valorMaximo: number, periodicidade: string) {
        this.CodigoServico = codigoServico;
        this.Servico = servico;
        this.Unidade = unidade;
        this.DataVigencia = dataVigencia;
        this.ValorMaximo = valorMaximo;
        this.Periodicidade = periodicidade;
    }

}