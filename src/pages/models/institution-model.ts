class InstitutionModel {
    Cnpj: string;
    Nome: string;

    constructor(cnpj: string, nome: string) {
        this.Cnpj = cnpj;
        this.Nome = nome;
    }

}