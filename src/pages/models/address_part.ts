class AddressPart {
    long_name: string;
    short_name: string;
    types: Array<string>;

    constructor(long_name: string, short_name: string, types: Array<string>) {
        this.long_name = long_name;
        this.short_name = short_name;
        this.types = types;
    }

}