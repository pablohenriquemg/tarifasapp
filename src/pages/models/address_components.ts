class AddressComponent {
    address_components: Array<AddressPart>;
    formatted_address: string;

    constructor(address_components: Array<AddressPart>, formatted_address: string) {
        this.address_components = address_components;
        this.formatted_address = formatted_address;
    }

}