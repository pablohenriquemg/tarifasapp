class OmbudsmanContactModel {
    CNPJ: string;
    Nome: string;
    Ouvidor: string;
    WebSite: string;
    Telefone: string;

    constructor(cnpj: string, nome: string, ouvidor: string, website: string, telefone: string) {
        this.CNPJ = cnpj;
        this.Nome = nome;
        this.Ouvidor = ouvidor;
        this.WebSite = website;
        this.Telefone = telefone;
    }

}