class EletronicUnitModel {
    Cnpj: string;
    NomeIf: string;
    Segmento: string;
    NomePosto: string;
    TipoPosto: string;
    Endereco: string;
    Numero: string;
    Complemento: string;
    Bairro: string;
    Cep: string;
    MunicipioIbge: string;
    Municipio: string;
    UF: string;
    CnpjAssist: string;
    NomeAssist: string;

    constructor(cnpj: string, nome: string, segmento: string, nomePosto: string,
        tipoPosto: string, endereco: string, numero: string, complemento: string,
        bairro: string, cep: string, municipioIbge: string, municipio: string,
        uf: string, cnpjAssist: string, nomeAssist: string) {
        this.Cnpj = cnpj;
        this.NomeIf = nome;
        this.Segmento = segmento;
        this.NomePosto = nomePosto;
        this.TipoPosto = tipoPosto;
        this.Endereco = endereco;
        this.Numero = numero;
        this.Complemento = complemento;
        this.Bairro = bairro;
        this.Cep = cep;
        this.MunicipioIbge = municipioIbge;
        this.Municipio = municipio;
        this.UF = uf;
        this.CnpjAssist = cnpjAssist;
        this.NomeAssist = nomeAssist;
    }

}