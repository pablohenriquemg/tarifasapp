import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';
import { Globals } from './globals';
import { Dialogs } from '@ionic-native/dialogs';

@Injectable()
export class RequestService {

    constructor(
        private http: HTTP,
        private loadingController: LoadingController,
        private globals: Globals,
        private dialogs: Dialogs
    ) { }

    getAllConsolidatedGroups(): Promise<ConsolidatedGroupModel[]> {
        let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_CONSOLIDATED_GROUPS;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, { 'format': 'JSON' }, { })
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);

                    if (jsondata.value.length < 1) {
                        this.dialogs.alert(this.globals.NO_CONSOLIDATED_GROUPS, this.globals.OPS);
                    }

                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getLastSelicRate(): Promise<RateModel[]> {
        let completeUrl = this.globals.URL_API_BCB_URL + this.globals.URL_LAST_SELIC_RATE;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, { 'format': 'JSON' }, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);

                    if (jsondata.length < 1) {
                        this.dialogs.alert(this.globals.FAIL_TO_GET_RATE, this.globals.OPS);
                    }
                    resolve(jsondata);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getLastIpcaRate(): Promise<RateModel[]> {
        let completeUrl = this.globals.URL_API_BCB_URL + this.globals.URL_LAST_IPCA_RATE;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, { 'format': 'JSON' }, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);

                    if (jsondata.length < 1) {
                        this.dialogs.alert(this.globals.FAIL_TO_GET_RATE, this.globals.OPS);
                    }

                    resolve(jsondata);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getOmbudsmanContact(ombudsmanName: string): Promise<OmbudsmanContactModel[]> {
        let filterParam = "contains(Nome," + "'" + ombudsmanName + "'" + ")";
        let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_OMBUDSMAN_CONTACT;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, {
                '$filter': filterParam,
                'top': '10', 'format': 'JSON',
                '$select': 'CNPJ,Nome,Ouvidor,WebSite,Telefone'
            }, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);

                    if (jsondata.value.length < 1) {
                        this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS);
                    }

                    jsondata.value.forEach(item => {
                        let cnpj = item.CNPJ.substring(0, 2).concat(".");
                        cnpj += item.CNPJ.substring(2, 5).concat(".");
                        cnpj += item.CNPJ.substring(5, 8).concat("/****-**");
                        item.CNPJ = cnpj;
                    });

                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getInstitutionsByConsolidatedGroup(groupCode: string): Promise<InstitutionModel[]> {
        let paramGroupCode = "'".concat(groupCode).concat("'");
        let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_CONSOLIDATED_INSTITUTIONS;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, {
                'top': '100', 'format': 'JSON',
                '$select': 'Cnpj,Nome',
                '@CodigoGrupoConsolidado': paramGroupCode
            }, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);

                    if (jsondata.length < 1) {
                        this.dialogs.alert(this.globals.FAIL_TO_GET_INSTITUTIONS, this.globals.OPS);
                    }

                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getRateByInstitutions(personType: string, cnpj: string): Promise<InstitutionRateModel[]> {
        let personSelected = "'".concat(personType).concat("'");
        let cnpjSelected = "'".concat(cnpj).concat("'");
        let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_RATES_BY_INSTITUTION;

        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, {
                'format': 'JSON',
                '@PessoaFisicaOuJuridica': personSelected,
                '@CNPJ': cnpjSelected
            }, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);
                    if (jsondata.length < 1) {
                        this.dialogs.alert(this.globals.FAIL_TO_GET_RATE, this.globals.OPS);
                    }
                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    getEletronicUnits(institution: string, address: string, city: string): Promise<EletronicUnitModel[]> {

        let filter = this.makeFilter(encodeURI(institution), encodeURI(address), encodeURI(city));
        let completeUrl = "https://olinda.bcb.gov.br/olinda/servico/Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico?$top=30&$format=JSON&$select=Cnpj,NomeIf,Segmento,NomePosto,TipoPosto,Endereco,Numero,Complemento,Bairro,Cep,MunicipioIbge,Municipio,UF,CnpjAssist,NomeAssist&$filter=";
        completeUrl = completeUrl.concat(filter);

        //let filter = this.makeFilter(encodeURI(institution), encodeURI(address), encodeURI(city));
        //let completeUrl = this.globals.BASE_URL_END_POINT + this.globals.URL_ELETRONIC_UNITS;
        let loading = this.loadingController.create({ content: this.globals.PROGRESS_MESSAGE });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, {}, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);
                    if (jsondata.value.length == 0) {
                        this.dialogs.alert(this.globals.NO_ELETRONIC_UNIT_FIND, this.globals.OPS);
                    }
                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    makeFilter(institution: string, address: string, city: string) {
        let filterInstitution = "";
        let filterAddress = "";
        let filterCity = "";

        if (!this.isNullOrEmpty(institution)) {
            filterInstitution = "contains(NomeIf,".concat("'").concat(institution.toUpperCase()).concat("')");
        }

        if (!this.isNullOrEmpty(address)) {
            filterAddress = "contains(Endereco,".concat("'").concat(address.toUpperCase()).concat("')");
        }

        if (!this.isNullOrEmpty(city)) {
            filterCity = "contains(Municipio,".concat("'").concat(city.toUpperCase()).concat("')");
        }

        let filter = "";

        if (!this.isNullOrEmpty(filterInstitution)) {
            filter = filterInstitution;
            if (!this.isNullOrEmpty(filterAddress)) {
                filter = filter.concat(" and ").concat(filterAddress);
            }
            if (!this.isNullOrEmpty(filterCity)) {
                filter = filter.concat(" and ").concat(filterCity);
            }
            return filter;
        }

        if (!this.isNullOrEmpty(filterAddress)) {
            filter = filterAddress;
            if (!this.isNullOrEmpty(filterInstitution)) {
                filter = filter.concat(" and ").concat(filterInstitution);
            }
            if (!this.isNullOrEmpty(filterCity)) {
                filter = filter.concat(" and ").concat(filterCity);
            }
            return filter;
        }

        if (!this.isNullOrEmpty(filterCity)) {
            filter = filterCity;
            if (!this.isNullOrEmpty(filterInstitution)) {
                filter = filter.concat(" and ").concat(filterInstitution);
            }
            if (!this.isNullOrEmpty(filterAddress)) {
                filter = filter.concat(" and ").concat(filterAddress);
            }
            return filter;
        }

    }

    getEletronicUnitsByAddressAndCity(address: string, city: string): Promise<EletronicUnitModel[]> {
        let filter = this.makeFilterForSimpleAddress(encodeURI(address), encodeURI(city));
        let completeUrl = "https://olinda.bcb.gov.br/olinda/servico/Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico?$top=30&$format=JSON&$select=Cnpj,NomeIf,Segmento,NomePosto,TipoPosto,Endereco,Numero,Complemento,Bairro,Cep,MunicipioIbge,Municipio,UF,CnpjAssist,NomeAssist&$filter=";
        completeUrl = completeUrl.concat(filter);
        let loading = this.loadingController.create({ content: 'Obtendo os postos eletrônicos próximos...' });
        loading.present();
        return new Promise((resolve, reject) => {
            this.http.get(completeUrl, {}, {})
                .then(data => {
                    loading.dismissAll();
                    let jsondata = JSON.parse(data.data);
                    if (jsondata.value.length == 0) {
                        this.dialogs.alert('Não foi encontrado nenhum posto eletrônico próximo, por favor tente outra opção de pesquisa.', 'Ops');
                    }
                    resolve(jsondata.value);
                })
                .catch(error => {
                    loading.dismissAll();
                    reject(error.error);
                });
        });
    }

    makeFilterForSimpleAddress(address: string, city: string) {
        let filterAddress = "";
        let filterCity = "";

        if (!this.isNullOrEmpty(address)) {
            filterAddress = "contains(Endereco,".concat("'").concat(address.toUpperCase()).concat("')");
        }

        if (!this.isNullOrEmpty(city)) {
            filterCity = "contains(Municipio,".concat("'").concat(city.toUpperCase()).concat("')");
        }

        return filterAddress.concat(" and ").concat(filterCity);

    }

    isNullOrEmpty = function (value) {
        return (!value || value == undefined || value == "" || value.length == 0);
    }
}