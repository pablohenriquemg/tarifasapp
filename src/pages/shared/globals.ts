import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    CONSOLIDATED_GROUP_SELECTED: any = { };
    PERSON_TYPE_SELECTED: any = { id: 1, item: 'Pessoa Física', desc: 'F' };
    INSTITUTION_SELECTED: any = { };
    ELETRONIC_UNIT_SEARCH: any = { };
    URL_API_BCB_URL: string = 'http://api.bcb.gov.br/';
    BASE_URL_END_POINT: string = 'https://olinda.bcb.gov.br/olinda/servico/';
    URL_SELIC_DAY_RATE: string = 'dados/serie/bcdata.sgs.11/dados/ultimos/30';
    URL_SELIC_MONTH_RATE: string = 'dados/serie/bcdata.sgs.4390/dados/ultimos/12';
    URL_SELIC_YEAR_RATE: string = 'dados/serie/bcdata.sgs.1178/dados/ultimos/10';
    URL_LAST_SELIC_RATE: string = 'dados/serie/bcdata.sgs.1178/dados/ultimos/365';
    URL_LAST_IPCA_RATE: string = 'dados/serie/bcdata.sgs.4447/dados/ultimos/20';
    URL_OMBUDSMAN_CONTACT: string = 'Informes_Ouvidorias/versao/v1/odata/Ouvidorias'
    URL_ELETRONIC_UNITS: string = 'Informes_PostosDeAtendimentoEletronico/versao/v1/odata/PostosAtendimentoEletronico';
    URL_CONSOLIDATED_INSTITUTIONS: string = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/ListaInstituicoesDeGrupoConsolidado(CodigoGrupoConsolidado=@CodigoGrupoConsolidado)';
    URL_CONSOLIDATED_GROUPS: string = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/GruposConsolidados';
    URL_RATES_BY_INSTITUTION: string = 'Informes_ListaTarifasPorInstituicaoFinanceira/versao/v1/odata/ListaTarifasPorInstituicaoFinanceira(PessoaFisicaOuJuridica=@PessoaFisicaOuJuridica,CNPJ=@CNPJ)';
    URL_VALUES_SERVICES_BANKING: string = 'Informes_ListaValoresDeServicoBancario/versao/v1/odata/ListaValoresServicoBancario(PessoaFisicaOuJuridica=@PessoaFisicaOuJuridica,CodigoGrupoConsolidado=@CodigoGrupoConsolidado)';
    ATTENTION: string = 'Atenção';
    OPS: string = 'Ops...';
    PROGRESS_MESSAGE: string = 'Obtendo dados. Aguarde...';
    FAIL_REQUEST_MESSAGE: string = 'Falha ao obter dados. Por favor, tente novamente mais tarde.';
    FILL_ALL_FIELDS: string = 'Preencha todos os campos.';
    NO_ELETRONIC_UNIT_FIND: string = 'Não foi encontrado nenhum resultado com os critérios de busca.';
    NO_CONSOLIDATED_GROUPS: string = 'Não foi possível obter os grupos consolidados, por favor tente mais tarde.';
    FAIL_TO_GET_RATE: string = 'Falha para obter as taxas. Por favor, tente novamente mais tarde.';
    FAIL_TO_GET_INSTITUTIONS: string = 'Falha para obter as instituições. Por favor, tente novamente mais tarde.';
    FILL_ONLY_ONE_FIELD: string = 'Preencha pelo menos um critério de busca.';
    NO_RATE_INSTITUTION: string = 'Não foram localizadas tarifas para essa instituição.';
}