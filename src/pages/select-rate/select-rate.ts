import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SelicPage } from '../selic/selic';
import { IpcaPage } from '../ipca/ipca';

@Component({ templateUrl: 'select-rate.html' })
export class SelectRatePage {

    constructor(private navCtrl: NavController) { }

    onButtonClick(rate: string) {

        switch (rate) {
            case 'selic': {
                this.navCtrl.push(SelicPage, {});
                break;
            }
            case 'ipca': {
                this.navCtrl.push(IpcaPage, {});
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    }

}