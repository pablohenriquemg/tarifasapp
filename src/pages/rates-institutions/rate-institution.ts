import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { Globals } from '../shared/globals';
import { RequestService } from '../shared/req-service';
import { ConsolidatedGroupsPage } from '../consolidated-groups/consolidated-groups';

@Component({ templateUrl: 'rate-institution.html' })
export class RateInstitutionPage {

    institution_rates: Array<InstitutionRateModel>;
    temporary_institution_rates: Array<InstitutionRateModel>;
    selected_institution: string;
    selected_person: string;

    constructor(private navCtrl: NavController,
        private dialogs: Dialogs,
        private globals: Globals,
        private rqService: RequestService) { }

    ionViewDidLoad() {
        this.selected_institution = this.globals.INSTITUTION_SELECTED.Nome;
        this.selected_person = this.globals.PERSON_TYPE_SELECTED.item;
        this.getAllItems();
    }

    getAllItems() {

        let personType = this.globals.PERSON_TYPE_SELECTED.desc;
        let cnpj = this.globals.INSTITUTION_SELECTED.Cnpj;

        this.rqService.getRateByInstitutions(personType, cnpj).then((data) => {

            if (data.length === 0) {
                this.dialogs.alert(this.globals.NO_RATE_INSTITUTION, this.globals.OPS)
                    .then(() => this.navCtrl.push(ConsolidatedGroupsPage));
            } else {
                data.forEach((item) => { item.Periodicidade = item.Periodicidade.toUpperCase(); })
                this.institution_rates = data;
                this.temporary_institution_rates = data;
            }

        }).catch(error => {
            this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
                .then(() => this.navCtrl.push(ConsolidatedGroupsPage));
        });

    }

    getItems(ev) {
        // Reset items back to all of the items
        this.institution_rates = this.temporary_institution_rates;
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.institution_rates = this.institution_rates.filter((item) => {
                return (item.Servico.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

}