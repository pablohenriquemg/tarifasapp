import { Component } from '@angular/core';
import { ConsolidatedGroupsPage } from '../consolidated-groups/consolidated-groups'
import { SelectRatePage } from '../select-rate/select-rate'
import { OmbudsmanContactPage } from '../ombudsman-contact/ombudsman-contact'
import { EletronicUnitPostPage } from '../eletronic-unit-post/eletronic-unit-post'

@Component({ templateUrl: 'tabs.html' })
export class TabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root = ConsolidatedGroupsPage;
    tab2Root = OmbudsmanContactPage;
    tab3Root = EletronicUnitPostPage;
    tab4Root = SelectRatePage;

    constructor() {

    }
}