import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Globals } from '../shared/globals';
import { Dialogs } from '@ionic-native/dialogs';
import { RequestService } from '../shared/req-service';

@Component({ templateUrl: 'ombudsman-contact.html' })
export class OmbudsmanContactPage {

    ombudsmanContactList: Array<OmbudsmanContactModel> = [];

    constructor(private dialogs: Dialogs,
        private globals: Globals,
        private plt: Platform,
        private rqService: RequestService) { }

    getOmbudsmanName(ombudsmanName: string) {
        if (this.plt.is('ios') || this.plt.is('android')) {
            this.rqService.getOmbudsmanContact(ombudsmanName.trim()).then((data) => {
                this.ombudsmanContactList = data;
            }).catch(error => {
                this.dialogs.alert(this.globals.FAIL_REQUEST_MESSAGE, this.globals.OPS)
            });
        }
    }

    search(ev) {
        var val = ev.target.value;
        this.ombudsmanContactList = [];
        if (val) {
            this.getOmbudsmanName(val.toUpperCase());
        }
    }

}