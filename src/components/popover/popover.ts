import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { Globals } from '../../pages/shared/globals';
import { ToastController } from 'ionic-angular';

@Component({ selector: 'popover', templateUrl: 'popover.html' })
export class PopoverComponent {

	items: any;

	constructor(private viewCtrl: ViewController,
		private globals: Globals,
		private toastCtrl: ToastController) { }

	ionViewDidLoad() {
		this.items = [
			{ id: 1, item: 'Pessoa Física', desc: 'F' },
			{ id: 2, item: 'Pessoa Jurídica', desc: 'J' },
		]
	}

	itemClick(obj) {
		this.presentToast(obj.item);
		this.globals.PERSON_TYPE_SELECTED = obj;
		this.viewCtrl.dismiss(obj);
	}

	presentToast(selectedPerson: string) {
		let toast = this.toastCtrl.create({
			message: selectedPerson.concat(' selecionada.'),
			duration: 3000,
			position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

}
