import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ChartModule } from 'angular2-highcharts';
import * as highcharts from 'Highcharts';
import { MomentModule } from 'ngx-moment';

import { MyApp } from './app.component';
import { RequestService } from '../pages/shared/req-service';
import { Geolocation } from '@ionic-native/geolocation';
import { Globals } from '../pages/shared/globals';
import { ConsolidatedGroupsPage } from '../pages/consolidated-groups/consolidated-groups';
import { RateInstitutionPage } from '../pages/rates-institutions/rate-institution';
import { InstitutionPage } from '../pages/institutions/institution';
import { SelicPage } from '../pages/selic/selic';
import { IpcaPage } from '../pages/ipca/ipca';
import { SelectRatePage } from '../pages/select-rate/select-rate';
import { OmbudsmanContactPage } from '../pages/ombudsman-contact/ombudsman-contact';
import { EletronicUnitPostPage } from '../pages/eletronic-unit-post/eletronic-unit-post';
import { EletronicUnitsPage } from '../pages/eletronic-units/eletronic-units';
import { NextPage } from '../pages/next/next';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HTTP } from '@ionic-native/http';
import { Dialogs } from '@ionic-native/dialogs';
import { PopoverComponent } from '../components/popover/popover';
import { TabsPage } from '../pages/tabs/tabs';
import { SelicModalPage } from '../pages/selic/selic-modal';
import { IpcaModalPage } from '../pages/ipca/ipca-modal';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    ConsolidatedGroupsPage,
    RateInstitutionPage,
    InstitutionPage,
    SelicPage,
    IpcaPage,
    SelectRatePage,
    OmbudsmanContactPage,
    EletronicUnitPostPage,
    EletronicUnitsPage,
    SelicModalPage,
    IpcaModalPage,
    NextPage,
    PopoverComponent
  ],
  imports: [
    BrowserModule,
    MomentModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      platforms: {
        ios: {
          tabsPlacement: 'bottom',
        }
      }
    }),
    ChartModule.forRoot(highcharts)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    ConsolidatedGroupsPage,
    RateInstitutionPage,
    InstitutionPage,
    SelicPage,
    IpcaPage,
    SelectRatePage,
    OmbudsmanContactPage,
    EletronicUnitPostPage,
    EletronicUnitsPage,
    SelicModalPage,
    IpcaModalPage,
    NextPage,
    PopoverComponent
  ],
  providers: [
    HTTP,
    Dialogs,
    StatusBar,
    SplashScreen,
    Geolocation,
    Globals,
    RequestService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
